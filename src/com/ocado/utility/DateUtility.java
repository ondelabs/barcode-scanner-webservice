package com.ocado.utility;

import java.util.Calendar;
import java.util.Date;

public class DateUtility
{
    public static Date addHours(Date date, int hours)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, hours); //minus number would decrement the days
        return cal.getTime();
    }
}
