package com.ocado.utility;

import java.util.logging.Logger;

import com.ocado.barcodescanner.UploadImageServlet;

public class BarcodeDecodeState {
	
	private static final Logger log =
			Logger.getLogger(UploadImageServlet.class.getName());
	
	public enum BarcodeDecodeEnumState {
		waiting, inProgress, done
	}
	
	public static String enumToString(BarcodeDecodeEnumState state) {
		switch (state) {
		case waiting:
			return "waiting";
		case inProgress:
			return "inProgress";
		case done:
			return "done";
		default:
			log.warning("Undefined enum value for BarcodeDecodeEnumState state");
			return "Error";
		}
	}
}