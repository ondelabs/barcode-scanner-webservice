package com.ocado.barcodescanner;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;

public class ClearExpiredData extends HttpServlet {

	private static final Logger log =
			Logger.getLogger(UploadImageServlet.class.getName());
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String barcodeName = "barcodeTable";//req.getParameter("guestbookName");
        Key barcodeKey = KeyFactory.createKey("barcodes", barcodeName);
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query query = new Query("Barcode", barcodeKey).addSort("expires_on", Query.SortDirection.ASCENDING);
        List<Entity> barcodes = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
        
        if (barcodes.size() > 0) {
        	for (int i = 0; i < barcodes.size(); ++i) {
        		Entity entity = barcodes.get(i);
        		Date expiresOn = (Date)entity.getProperty("expires_on");
        		
        		Date now = new Date();
        		if (now.compareTo(expiresOn) > 0) {
        			// delete blob store
        			String blobKeyString = entity.getProperty("blobkey").toString();
        			BlobKey blobKey = new BlobKey(blobKeyString);
        		    BlobstoreServiceFactory.getBlobstoreService().delete(blobKey);
        		    log.info("deleted a blob store");
        		    // Delete data store
        		    datastore.delete(entity.getKey());
        		    log.info("deleted a data store");
        		} else {
        			log.info("Data store hasn't expired yet.");
        			break;
        		}
        	}
        } else {
			log.info("No data stores present");
		}
        
        response.setStatus(200);
	}
}
