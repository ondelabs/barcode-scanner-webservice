package com.ocado.barcodescanner;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ar.com.hjg.pngj.ImageLine;
import ar.com.hjg.pngj.PngReader;
import ar.com.hjg.pngj.PngjException;
import ar.com.hjg.pngj.PngjInputException;
import ar.com.hjg.pngj.PngjOutputException;

import com.google.appengine.api.ThreadManager;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileReadChannel;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.oned.MultiFormatOneDReader;
import com.ocado.utility.BarcodeDecodeState.BarcodeDecodeEnumState;

public class BarcodeDecrypt extends HttpServlet{

	private static final Logger log =
			Logger.getLogger(UploadImageServlet.class.getName());
	
	public void doGet(HttpServletRequest request, final HttpServletResponse resp) {
		String instanceType = request.getParameter("instance_type");
		
		if (instanceType.equalsIgnoreCase("backend") == true) {
			log.info("Backend initialising");
			Thread thread = ThreadManager.createBackgroundThread(new Runnable() {
				public void run() {
					log.info("Backend processing");
					ProcessDecryptionOfBarcodes();
				}
			});
			thread.start();
			log.info("Backend started");
		} else {
			log.info("Running decrypt on frontend");
			ProcessDecryptionOfBarcodes();
		}
		
		resp.setStatus(200);
	}

	private void ProcessDecryptionOfBarcodes() {
        String barcodeName = "barcodeTable";//req.getParameter("guestbookName");
        Key barcodeKey = KeyFactory.createKey("barcodes", barcodeName);
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter stateFilter = new Query.FilterPredicate("state", FilterOperator.EQUAL, BarcodeDecodeEnumState.waiting.toString());
        Query query = new Query("Barcode", barcodeKey).setFilter(stateFilter);
        List<Entity> barcodes = datastore.prepare(query).asList(FetchOptions.Builder.withDefaults());
        
        if (barcodes.size() > 0) {
        	for (int i = 0; i < barcodes.size(); ++i) {
        		Entity entity = barcodes.get(i);
        		entity.setProperty("state", BarcodeDecodeEnumState.inProgress.toString());
        		datastore.put(entity);
        		
        		String blobKeyString = entity.getProperty("blobkey").toString();
        		
        		if (blobKeyString == null) {
        			log.warning("Trying to process data store with a null blob key reference");
        		} else {
        			BlobKey blobKey = new BlobKey(blobKeyString);
        			
                    FileService fileService = FileServiceFactory.getFileService();
                    AppEngineFile blobFile = fileService.getBlobFile(blobKey);
                    FileReadChannel readChannel = null;
					try {
						readChannel = fileService.openReadChannel(blobFile, false);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
                    byte[] imageData = null;
                    InputStream stream = Channels.newInputStream(readChannel);
                    PngReader pngr = null;
                    try {
                    	pngr = new PngReader(stream, null);
                    } catch (PngjInputException e) {
                    	log.warning("PngjInputException. " + e.getMessage());
                    	pngr = null;
                    } catch (PngjOutputException e) {
                    	log.warning("PngjOutputException. " + e.getMessage());
                    	pngr = null;
                    } catch (PngjException e) {
                    	log.warning("PngjException. " + e.getMessage());
                    	pngr = null;
                    }
                    if (pngr == null) {
                    	entity.setProperty("result", "Internal server error. Try again.");
                    } else {
	                    if (pngr.imgInfo.channels < 3) {
	                    	entity.setProperty("result", "error, png channel count smaller than 3");
	                    } else {
							try {
								imageData = getByteArray(pngr);
								pngr.end();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
		    				
							RGBLuminanceSource source = new RGBLuminanceSource(imageData, pngr);
		    				BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
		    				
		    				HashMap<DecodeHintType, Collection<BarcodeFormat>> hints = new HashMap<DecodeHintType, Collection<BarcodeFormat>>();
		    				List<BarcodeFormat> formats = Arrays.asList(BarcodeFormat.CODE_39, BarcodeFormat.CODE_93, BarcodeFormat.CODE_128, BarcodeFormat.EAN_8, BarcodeFormat.EAN_13,
		    						BarcodeFormat.ITF, BarcodeFormat.RSS_14, BarcodeFormat.RSS_EXPANDED, BarcodeFormat.UPC_A, BarcodeFormat.UPC_E, BarcodeFormat.UPC_EAN_EXTENSION);
		    				hints.put(DecodeHintType.POSSIBLE_FORMATS, formats);
		    				hints.put(DecodeHintType.TRY_HARDER, null);
		
		    				MultiFormatOneDReader reader = new MultiFormatOneDReader(hints);
		    				Result result = null;
		    				try {
			    				HashMap<DecodeHintType, Boolean> tryHarderHint = new HashMap<DecodeHintType, Boolean>();
			    				tryHarderHint.put(DecodeHintType.TRY_HARDER, true);
			    				
		    					result = reader.decode(bitmap, tryHarderHint);
		    					log.info(result.getText());
		    					log.info(result.getBarcodeFormat().toString());
		    					entity.setProperty("result", result.getText());
		    				} catch (ReaderException e) {
		    					// the data is improperly formatted
		    					log.warning("the data is improperly formatted. " + e.getMessage());
		    					entity.setProperty("result", "Barcode not found");
		    				}
	                    }
                    }
                    try {
						stream.close();
					} catch (IOException e) {
						log.warning(e.getMessage());
					}
        		}
				entity.setProperty("state", BarcodeDecodeEnumState.done.toString());
				datastore.put(entity);
        	}
        } else {
			log.info("No data stores present");
		}
	}
	
	private static byte[] getByteArray(PngReader pngr) throws IOException {
		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		
        for (int row = 0; row < pngr.imgInfo.rows; ++row) {
        	ImageLine l1 = pngr.readRow(row);
        	// Convert from int array to byte array
        	byte[] byteRow = new byte[l1.scanline.length];
        	for (int i = 0; i < byteRow.length; ++i) {
        		byteRow[i] = (byte) Math.abs(l1.scanline[i]);
        	}
        	buffer.write(byteRow, 0, byteRow.length);
        }
	    
	    buffer.flush();
	    buffer.close();
	    return buffer.toByteArray();
	}
}
