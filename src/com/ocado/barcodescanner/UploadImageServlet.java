package com.ocado.barcodescanner;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.appengine.api.backends.BackendService;
import com.google.appengine.api.backends.BackendServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.files.AppEngineFile;
import com.google.appengine.api.files.FileService;
import com.google.appengine.api.files.FileServiceFactory;
import com.google.appengine.api.files.FileWriteChannel;
import com.ocado.response.ErrorResponse;
import com.ocado.response.Response;
import com.ocado.response.UploadImageResponse;
import com.ocado.utility.BarcodeDecodeState.BarcodeDecodeEnumState;
import com.ocado.utility.DateUtility;

public class UploadImageServlet extends HttpServlet {
	
  private static final Logger log =
      Logger.getLogger(UploadImageServlet.class.getName());

  public void doPost(HttpServletRequest req, HttpServletResponse res)
      throws ServletException, IOException {
	  Response response = null;
    try {
      ServletFileUpload upload = new ServletFileUpload();
      res.setContentType("text/plain");

      FileItemIterator iterator = upload.getItemIterator(req);
      while (iterator.hasNext()) {
        FileItemStream item = iterator.next();
        InputStream stream = item.openStream();

        if (item.isFormField()) {
          log.info("Got a form field: " + item.getFieldName());
        } else {
          log.info("Got an uploaded file: " + item.getFieldName() +
                      ", name = " + item.getName());
          
          if (item.getContentType().contains("png")) {
	          // Get a file service
	          FileService fileService = FileServiceFactory.getFileService();
	          // Create a new Blob file with mime-type "text/plain"
	          AppEngineFile file = fileService.createNewBlobFile(item.getContentType(), item.getName());

	          // Open a channel to write to it
	          boolean lock = true;
	          FileWriteChannel writeChannel = fileService.openWriteChannel(file, lock);
	          // copy byte stream from request to channel
	          byte[] buffer = new byte[10000];
	          int len;
	          while ((len = stream.read(buffer)) > 0) {
	              writeChannel.write(ByteBuffer.wrap(buffer, 0, len));
	          }
	          writeChannel.closeFinally();
	          String blobKey = fileService.getBlobKey(file).getKeyString();
	          
	          // Store the barcode data into the datastore
	          String barcodeName = "barcodeTable";//req.getParameter("guestbookName");
	          Key barcodeKey = KeyFactory.createKey("barcodes", barcodeName);
	          Date date = new Date();
	          date = DateUtility.addHours(date, 1);
	          Entity barcode = new Entity("Barcode", barcodeKey);
	          barcode.setProperty("blobkey", blobKey);
	          barcode.setProperty("expires_on", date);
	          barcode.setProperty("state", BarcodeDecodeEnumState.waiting.toString());
	          barcode.setProperty("result", "");
	          DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
	          datastore.put(barcode);
	         
	          CallBackend(res);
	          
	          // respond to query
	          res.setContentType("text/plain");
	          response = new UploadImageResponse(blobKey, date.toString());
	        } else {
	        	response = new ErrorResponse(415, "image must be a png");
	        }
        }
      }
    } catch (Exception ex) {
    	response = new ErrorResponse(500, ex.getMessage());
    	throw new ServletException(ex);
    } finally {
    	res.getOutputStream().write(response.toJson().getBytes());
    }
  }
  
  private void CallBackend(HttpServletResponse Response) throws IOException {
	  BackendService backendsApi = BackendServiceFactory.getBackendService();
	  String backendAddress = backendsApi.getBackendAddress("barcodedecrypt");
	  if (!backendAddress.contains("http://")) {
		  backendAddress = "http://"+backendAddress;
	  }

	  URL url = new URL(backendAddress+"/decrypt?instance_type=backend");
	  URLConnection connection = url.openConnection();
	  connection.connect();
	  
	  // Cast to a HttpURLConnection
	  if (connection instanceof HttpURLConnection) {
		  int code = 200;
		  boolean timedOut = false;
		 try {
		     HttpURLConnection httpConnection = (HttpURLConnection)connection;
		     code = httpConnection.getResponseCode();
		     httpConnection.disconnect();
		 } catch(SocketTimeoutException e) {
			 log.warning("error - timed out while informing backend to decrypt.");
			 timedOut = true;
		 }

	     if (code == 404 || timedOut == true) {
	    	 log.warning("Backend instance is down or another such backend error. Trying decrypt on frontend instance.");
	    	 String frontendAddress = null;
	    	 // This check is basically for when running on development, do this:
	    	 if (backendAddress.contains("decrypt-barcode.appspot.com")) {
	    		 frontendAddress = "http://decrypt-barcode.appspot.com";
	    	 } else { // When running locally do this:
	    		 frontendAddress = backendAddress;
	    	 }

	    	 try {
				URL frontendURL = new URL(frontendAddress + "/decrypt?instance_type=frontend");
				URLConnection frontendConnection = frontendURL.openConnection();
				frontendConnection.connect();
				HttpURLConnection frontendHttpConnection = (HttpURLConnection)frontendConnection;

				if (frontendHttpConnection.getResponseCode() != 200) {
					log.warning("error - Can't decrypt barcode.");
				}
				  
				frontendHttpConnection.disconnect();
	    	 } catch(SocketTimeoutException e) {
	    		 log.warning("error - timed out while decrypting.");
	    	 }
	     } else {
	    	 log.info("Decryption of the barcode is being performed on the backend.");
	     }
	  } else {
	     log.warning("error - not a http request!");
	  }
  }
}