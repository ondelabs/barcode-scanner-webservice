package com.ocado.barcodescanner;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.ocado.response.BarcodeStateResultResponse;
import com.ocado.response.ErrorResponse;
import com.ocado.response.Response;
import com.ocado.utility.BarcodeDecodeState;

public class BarcodeStateResultServlet extends HttpServlet {
	
	private static final Logger log =
			Logger.getLogger(UploadImageServlet.class.getName());
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String blobParameter = request.getParameter("key");
	    if (blobParameter == null) {
	        response.sendError(400, "Missing 'key' parameter.");
	    }
		
        String barcodeName = "barcodeTable";//req.getParameter("guestbookName");
        Key barcodeKey = KeyFactory.createKey("barcodes", barcodeName);
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter blobKeyFilter = new Query.FilterPredicate("blobkey", FilterOperator.EQUAL, blobParameter);
        Query query = new Query("Barcode", barcodeKey).setFilter(blobKeyFilter);
        List<Entity> barcodes = datastore.prepare(query).asList(FetchOptions.Builder.withLimit(2));
        
        Response responseToSend = null;
        
        if (barcodes.size() == 1) {
        	Entity entity = barcodes.get(0);
        	String state = entity.getProperty("state").toString();
        	BarcodeDecodeState.BarcodeDecodeEnumState enumState = BarcodeDecodeState.BarcodeDecodeEnumState.valueOf(state);
        	String result = entity.getProperty("result").toString();
        	responseToSend = new BarcodeStateResultResponse(BarcodeDecodeState.enumToString(enumState), result);
        } else if (barcodes.size() > 1) {
        	log.warning("Too many values returned for the same blob key");
        	responseToSend = new ErrorResponse(500 , "key is not unique");
        } else {
        	log.warning("No data store present for the blob key that was passed in");
        	responseToSend = new ErrorResponse(404 , "no data with specified key");
        }
        
    	response.setContentType("text/plain");
    	response.getOutputStream().write(responseToSend.toJson().getBytes());
	}
}
