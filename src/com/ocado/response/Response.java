package com.ocado.response;

import com.google.gson.Gson;

public class Response {
	public String toJson() {
    	Gson gson = new Gson();
    	String json = gson.toJson(this);
    	return json;
	}
}
