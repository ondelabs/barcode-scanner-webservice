package com.ocado.response;

public class BarcodeStateResultResponse extends Response {
	private String _state;
	private String _result;

	public BarcodeStateResultResponse(String state, String result) {
		_state = state;
		_result = result;
	}
}
