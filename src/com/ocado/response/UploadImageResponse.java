package com.ocado.response;


public class UploadImageResponse extends Response {
	private String _key;
	private String _expiresOn;

	public UploadImageResponse(String key, String expiresOn) {
	    _key = key;
	    _expiresOn = expiresOn;
	}
}
