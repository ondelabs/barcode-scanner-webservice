package com.ocado.response;

public class ErrorResponse extends Response {
	private int _errorCode;
	private String _reason;

	public ErrorResponse(int errorCode, String reason) {
		_errorCode = errorCode;
		_reason = reason;
	}
}
